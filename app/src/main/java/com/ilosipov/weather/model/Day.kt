package com.ilosipov.weather.model

import com.google.gson.annotations.SerializedName

/**
 * Класс Day - модель погоды на один день
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 26.04.2020
 * @version $Id$
 */

data class Day(
    @SerializedName(value = "main")
    var main : Main? = null,
    @SerializedName(value = "weather")
    var weathers : List<Weather>? = null,
    @SerializedName(value = "wind")
    var wind : Wind? = null,
    @SerializedName(value = "dt_txt")
    var date : String = "") {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Day

        if (main != other.main) return false
        if (weathers != other.weathers) return false
        if (wind != other.wind) return false
        if (date != other.date) return false

        return true
    }

    override fun hashCode(): Int {
        var result = main?.hashCode() ?: 0
        result = 31 * result + (weathers?.hashCode() ?: 0)
        result = 31 * result + (wind?.hashCode() ?: 0)
        result = 31 * result + date.hashCode()
        return result
    }

    override fun toString(): String {
        return "\nDay:\nmain = $main,\nweathers = $weathers,\nwind = $wind,\ndate = $date"
    }
}