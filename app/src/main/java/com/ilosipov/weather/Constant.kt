package com.ilosipov.weather

const val REQUEST_LOCATION_PERMISSION = 1
const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
const val KEY_API = "c84121690e5dc5f95ec5a62b275b7306"
const val UNITS = "metric"