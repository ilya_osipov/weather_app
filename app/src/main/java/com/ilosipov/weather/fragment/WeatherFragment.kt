package com.ilosipov.weather.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ilosipov.weather.R

/**
 * Класс WeatherFragment - основной экран погоды
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 26.04.2020
 * @version $Id$
 */

class WeatherFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?) : View? {
        val view = inflater.inflate(R.layout.fragment_weather, container, false)
        return view
    }
}