package com.ilosipov.weather.activity

import androidx.fragment.app.Fragment
import com.ilosipov.weather.BaseActivity
import com.ilosipov.weather.fragment.WeatherFragment

/**
 * Класс MainActivity - стартовая активити
 * @author Ilya Osipov (mailto:il.osipov.gm@gmail.com)
 * @since 26.04.2020
 * @version $Id$
 */

class MainActivity : BaseActivity() {

    override fun createFragment() : Fragment = WeatherFragment()
}
