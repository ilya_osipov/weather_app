Weather App
[![Platform](https://img.shields.io/badge/platform-kotlin-yellow.svg)](https://kotlinlang.ru/)
[![Platform](https://img.shields.io/badge/platform-android-green.svg)](http://developer.android.com/index.html)
===========

Weather forecast application.

Description
-----------
Beautiful weather application. It is possible to look at the weather of current location, also find the location
and fin out more detailed weather for 5 days.

Preview
-------


Contact
-------
tel: +7 (911) 395-34-25  
telegram: [@il_osipov](https://t.me/il_osipov)  
e-mail: il.osipov.gm@gmail.com